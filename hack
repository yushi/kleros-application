1.
sendToken() fails to verify that the sender has suffienct balance. An attack could underflow her balance. Should use SafeMath.

2.
vote() has an overflow vulnerability. The attacker could pass in a large _nbVotes and cause an overflow to circumvent the check.

3.
buyToken() has an overflow vulnerability. The attacker could use overflow to circumvent the second require().

4.
The contract is vulnerable to DOS attack. The attacker could repeatedly call store(), paying only the gas prices, and bloat the safes array. Then take() would consume too much gas to execute. Also take() does not follow the checks-effects-interactions best practice.

5.
recordContribution() is a public method that the attacker could call to add contribution without paying.

6.
In sendAllTokens() the line balances[_recipient]=+balances[msg.sender]; should be balances[_recipient]+=balances[msg.sender];

7.
Users of the dApp could collude to gain unwarranted discount rate by using a proxy smart contract to call buy(). An attacker could use a smart contract to resell and gain a profit at the cost of the original seller.

8.
Because the states of smart contracts are on the blockchain, the guesser actually knows the value of lastChoiceHead.

9.
Subject to reentrancy attack. Should put the state-changing statement balances[msg.sender]=0; before transfering.

10.
This smart contract is subject to attack by miners. Before party B calls guess(), a miner could use the address of the account as coinbase to charge values into it. Then she would be able to call timeOut() repeatedly to drain the value, eventually steal the 1 ether that part A put in.

11.
A malicious miner could use the contract address as coinbase to break the "invariant" and then call invariantBroken() to steal funds.

