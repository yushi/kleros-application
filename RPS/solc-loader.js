// Webpack loader that compiles a .sol into a JSON containing its abi and bin 
// for Javascript modules to use(import).
const path = require('path');
const {execSync} = require('child_process');

module.exports = function () {
    return execSync('solc --combined-json abi,bin '+path.relative(this.context, this.resourcePath), {encoding: 'utf8'});
};
