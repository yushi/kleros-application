import {EventEmitter} from 'events';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import {contracts} from './RPS.sol';
const {abi: ABI, bin: BIN} = contracts['RPS.sol:RPS'];
import * as utils from './utils';

// window.Web3 comes from web3.js from CDN, see index.html
const web3 = new window.Web3(window.web3.currentProvider);

const MOVES = ['Rock', 'Paper', 'Scissors', 'Spock', 'Lizard'];
const ADDR_REGEXP = /^0x[a-fA-F0-9]{40}$/;
//Metamask has difficulty estimating gas of the j?Timeout methods
const TIMEOUT_METHOD_GAS = 40000; 

/* Architecture: 
                   transaction     'change' event
                     <---              --->
blockchain (web3.js)     class RPSState    class RPSGame <--> view
                     --->        ^     <---
                   poll state    |   event callbacks
                                 |
                               timer 
*/ 


// The state of the game
class RPSState extends EventEmitter {
    /* State diagram:
    NOT_CREATED--->J2_TURN--->J2_TIMEOUT
                       |
                       V
                   J2_PLAYED--->J1_TIMEOUT
                       |
                       V
                     SOLVED
    */
    //@param whichPlayer: integer 1 or 2, indicates which player this state object serves
    constructor (whichPlayer) {
        super();

        this.whichPlayer = whichPlayer;
        
        this.state = 'NOT_CREATED';
        // indicating a transaction is pending and when it commits state will change
        this.transitioning = false;

        this.contractAddress = null; // the address
        this.contract = null; // the web3 contract obj

        // contract state variables that we care about:
        this.lastAction = null;
        this.c2 = null;
        this.stake = null;
        this.timeout = null;

        this.timeoutTimer = null;
        this.pollTimer = null;
        this.POLL_INTERVAL = 15*1000; // no point polling faster than eth block time
    }

    // fires change event when timeout occurs 
    async watchTimeout () {
        const self = this;

        if (!self.timeoutTimer) {
            clearTimeout(self.timeoutTimer);
        }

        self.timeoutTimer = setTimeout(function () {
            self.fireChangeEvent();
        }, (self.lastAction + self.timeout)*1000-Date.now())
    }

    isTimeOut () {
        if (!this.lastAction || !this.timeout) {
            return false;
        }
        console.log('isTimeout', new Date, new Date((this.lastAction + this.timeout)*1000));
        return Date.now() > (this.lastAction + this.timeout)*1000;
    }

    // sync with the blockchain
    async sync () {
        const contract = this.contract;
        this.lastAction = Number(await utils.readContractProperty(contract, 'lastAction'));
        this.c2 = await utils.readContractProperty(contract, 'c2');
        this.stake = await utils.readContractProperty(contract, 'stake');
        this.timeout = Number(await utils.readContractProperty(contract, 'TIMEOUT'));

        var state;
        if (this.stake === '0') {
            state = 'SOLVED'; // actually could be J._TIMEOUT but we can't know
        } else if (this.c2 === '0') {
            state = 'J2_TURN';
        } else {
            state = 'J2_PLAYED';
        }
        this.enterState(state);
    }

    fireChangeEvent () {
        this.emit('change');
    }

    setContractAddress (a) {
        this.contractAddress = a;
        this.contract = new web3.eth.Contract(JSON.parse(ABI), a);
    }

    enterState (s) {
        const self = this;

        self.transitioning  = false;
        self.state = s;
        self.fireChangeEvent();

        clearInterval(self.pollTimer);
        clearTimeout(self.timeoutTimer);

        if (s==='J2_TURN' && self.whichPlayer===1) {
            self.watchPlayer2();
        }

        if (s==='J2_TURN' && self.whichPlayer===2) {
            self.watchPlayer1Timeout();
        }
        
        if (s==='J2_PLAYED' && self.whichPlayer===1) {
            self.watchPlayer2Timeout();
        }

        if (s==='J2_TURN' || s==='J2_PLAYED') {
            (async function () {
                self.lastAction = Number(await utils.readContractProperty(self.contract, 'lastAction'));
                if (!self.timeout) {
                    self.timeout = Number(await utils.readContractProperty(self.contract, 'TIMEOUT'));
                }
                self.watchTimeout();
            })();
        }

        if (s==='J2_PLAYED' && self.whichPlayer===2) {
            self.watchPlayer1Solve();
        }
    }

    setTransitioning (b) {
        this.transitioning = b;
        this.fireChangeEvent();
    }

    // player 1 calls this to create game
    onCreate (move, secret, peerAddress, stake) {
        const self = this;
        const salt = web3.utils.sha3(secret);
        const hash = web3.utils.soliditySha3({type: 'uint8', value: move}, 
                                             {type: 'uint256', value: salt});
        console.log('move', move, 'salt', salt, 'hash', hash);
        self.move = move;
        self.secret = secret;

        const contract = new web3.eth.Contract(JSON.parse(ABI));
        const resp = contract
            .deploy({data: BIN, arguments: [hash, peerAddress]})
            .send({from: utils.getMetamaskAddress(), 
                   value: web3.utils.toWei(stake, 'ether')}, 
                  function (err, txHash) { 
                  })
            .on('receipt', function (receipt) {
                self.setContractAddress(receipt.contractAddress);
                self.enterState('J2_TURN');
                })
            .on('error', function (error) {
                self.setTransitioning(false);
            });
        
        self.setTransitioning(true);
    }

    // player 1 calls this in J2_TURN to watch player 2's move
    watchPlayer2 () {
        const self = this;
        self.pollTimer = setInterval(function () {
            utils.readContractProperty(self.contract, 'c2').then(function (c2) {
                console.log('watch player2', typeof c2, c2);
                if (c2 !== '0') {
                    self.lastAction = null;
                    self.enterState('J2_PLAYED');
                }
            });
        }, self.POLL_INTERVAL);
    }

    // player 2 calls this in J2_TURN to watch player 1 calling timeout
    watchPlayer1Timeout () {
        const self = this;
        self.pollTimer = setInterval(function () {
            utils.readContractProperty(self.contract, 'stake').then(function (stake) {
                console.log('watch player1 timeout', typeof stake, stake);

                if (stake === '0') {
                    self.enterState('J2_TIMEOUT');
                }
            });
        }, self.POLL_INTERVAL);
    }

    // player 1 calls this in J2_PLAYED to watch player 2 calling timeout
    watchPlayer2Timeout () {
        const self = this;
        self.pollTimer = setInterval(function () {
            utils.readContractProperty(self.contract, 'stake').then(function (stake) {
                console.log('watch player2 timeout', typeof stake, stake);

                if (stake === '0') {
                    self.enterState('J1_TIMEOUT');
                }
            });
        }, self.POLL_INTERVAL);
    }

    watchPlayer1Solve () {
        const self = this;
        self.pollTimer = setInterval(function () {
            utils.readContractProperty(self.contract, 'stake').then(function (stake) {
                console.log('watch player1 solve', typeof stake, stake);

                if (stake === '0') {
                    self.enterState('SOLVED');
                }
            });
        }, self.POLL_INTERVAL);
    }

    onPlayer1CallTimeout () {
        const self = this;
        self.contract.methods.j2Timeout()
            .send({from: utils.getMetamaskAddress(),
                   gas: TIMEOUT_METHOD_GAS}, 
                  function (err, txHash) {})
            .on('receipt', function (receipt) {
                self.enterState('J2_TIMEOUT');
            })
            .on('error', function (error) {
                self.setTransitioning(false);
                self.sync();
            });
        self.setTransitioning(true);

    }

    // player 1 solves
    onSolve () {
        const self = this;

        const salt = web3.utils.sha3(self.secret);
        const move = self.move;

        self.contract.methods.solve(move, salt)
            .send({from: utils.getMetamaskAddress()}, 
                   function (err, txHash) {
                       if (!!err) {
                           alert(err.message);
                       }
                   })
            .on('receipt', function (receipt) {
                self.enterState('SOLVED');
            })
            .on('error', function (error) {
                self.setTransitioning(false);
                self.sync();
            });
        self.setTransitioning(true);
    }

    // playe 2 plays
    onPlay (move) {
        console.log('move', move);
        const self = this;
        self.contract.methods.play(move+1)
            .send({from: utils.getMetamaskAddress(), value: self.stake},
                 function (err, txHash) {})
            .on('receipt', function (receipt) {
                self.lastAction = Math.ceil(Date.now()/1000);
                self.enterState('J2_PLAYED');
            })
            .on('error', function (error) {
                self.setTransitioning(false);
                self.sync();
            });
        self.setTransitioning(true);
    }

    onPlayer2CallTimeout () {
        const self = this;
        self.contract.methods.j1Timeout()
            .send({from: utils.getMetamaskAddress(),
                   gas: TIMEOUT_METHOD_GAS}, 
                  function (err, txHash) {})
            .on('receipt', function (receipt) {
                self.enterState('J1_TIMEOUT');
            })
            .on('error', function (error) {
                self.setTransitioning(false);
                self.sync();
            });
        self.setTransitioning(true);
    }
}

// Binds RPSState to player1's view:
// 1. renders RPSState
// 2. fires view events towards RPSState
// Usage: <PRSGame1 state=<RPSState obj> />
class RPSGame1 extends Component {
    componentDidMount () {
        const self = this;
        this.props.state.on('change', function () {
            console.log('change event');
            self.forceUpdate();
        });
    }

    render () {
        const state = this.props.state;

        if (state.transitioning) {
            // or we could show a modal dialog
            return <div>Transaction pending...</div>
        }

        switch (state.state) {
        case 'NOT_CREATED':
            return <CreateGame onCreate={state.onCreate.bind(state)} />

        case 'J2_TURN': 
            return <WaitForPlayer2 contractAddress={state.contractAddress}
                                   onCallTimeout={state.onPlayer1CallTimeout.bind(state)}
                                   isTimeout={state.isTimeOut()} />

        case 'J2_TIMEOUT':
            return <div>The other player did not play. You have called timeout.</div>;

        case 'J2_PLAYED':
            return <Solve onSolve={state.onSolve.bind(state)} />
        case 'J1_TIMEOUT':
            return <div>You did not solve in time. The other player called timeout.</div>;
        
        case 'SOLVED':
            return <div>Game has terminated</div>;
        }
    }
}

class Solve extends Component {
    render () {
        return (<div>
           <div>Player2 has played</div>
           <div>
              <button onClick={this.props.onSolve}>Solve</button>
           </div>
        </div>);
    }
}

// Player 1 waits for player 2
// <WaitForPlayer2 contracAddress="0x..." onCallTimeout=function () isTimeOut=boolean/>
class WaitForPlayer2 extends Component {
    makePlayer2Url () {
        return document.location.href+'#player2/'+this.props.contractAddress;
    }

    render () {
        return (<div>
          <div>Please send this link to player 2 : {this.makePlayer2Url()}</div>
          <div>Waiting for player 2</div>
          <div>
            <button onClick={this.props.onCallTimeout} 
                    disabled={!this.props.isTimeout}>
              Timeout
            </button>
          </div>
        </div>);
    }
}

// Player 1 creates the game
// <CreateGame onCreate={function (move, secret, peerAddress, stake)}
class CreateGame extends Component {
    constructor (props) {
        super(props);
        const secret = web3.utils.randomHex(32).slice(2);
        this.state = {move:0, secret: secret};

        this.onMoveChange = this.onMoveChange.bind(this);
        this.onAddressChange = this.onAddressChange.bind(this);
        this.onCreate = this.onCreate.bind(this);
        this.onStakeChange = this.onStakeChange.bind(this);
        this.onSecretChange = this.onSecretChange.bind(this);
    }

    onMoveChange (i) {
        this.setState({move: i});
    }
    
    onStakeChange (event) {
        this.setState({stake: event.target.value});
    }

    onAddressChange (event) {
        const a = event.target.value;
        this.setState({address: a,
                       addressValid: ADDR_REGEXP.test(a)});
    }

    onSecretChange (event) {
        this.setState({secret: event.target.value});
    }

    onCreate () {
        this.props.onCreate(Number(this.state.move)+1, 
                            this.state.secret, 
                            this.state.address, 
                            this.state.stake);
    }
    
    render () {
        const self = this;
        return (<div>
            <div>
                <Move move={self.state.move} onMoveChange={self.onMoveChange} />
            </div>
            <div>
                stake: <input type="text" placeholder="ether" onChange={self.onStakeChange} />
            </div>
            <div>
                secret: <input type="text" placeholder="secret" value={this.state.secret} onChange={self.onSecretChange} />
            </div>    
            <div>
              <input type="text" placeholder="Player 2 address" onChange={self.onAddressChange} />
            </div>
            <div>
              <button onClick={self.onCreate} disabled={!self.state.addressValid}>Create Game</button>
            </div>
        </div>);
    }
}

class RPSGame2 extends Component {
    componentDidMount () {
        const self = this;
        this.props.state.on('change', function () {
            console.log('change event');
            self.forceUpdate();
        });
    }

    render () {
        const state = this.props.state;

        if (state.transitioning) {
            // or we could show a modal dialog
            return <div>Please wait...</div>
        }

        switch (state.state) {
        case 'J2_TURN':
            return <Player2Play onPlay={state.onPlay.bind(state)} stake={state.stake} />;
        case 'J2_TIMEOUT':
            return <div>You did not play. Player 1 has called timeout</div>;
        case 'J2_PLAYED':
            return <Player2Wait onTimeout={state.onPlayer2CallTimeout.bind(state)} 
                                isTimeOut={state.isTimeOut()}/>;
        case 'J1_TIMEOUT':
            return <div>Player 1 did not solve in time. You have called timeout.</div>;
        case 'SOLVED':
            return <div>Game has terminated</div>;
        }
    }
}

class Player2Play extends Component {
    constructor (props) {
        super(props);
        this.state = {move: 0};

        this.onMoveChange = this.onMoveChange.bind(this);
    }

    onMoveChange (i) {
        this.setState({move: i});
    }

    render () {
        return (<div>
          <Move move={this.state.move} onMoveChange={this.onMoveChange} />
          <div>
             stake: {web3.utils.fromWei(this.props.stake, 'ether')} ether
          </div>
          <div><button onClick={()=>this.props.onPlay(this.state.move)}>Play</button></div>
        </div>);
    }
}

class Player2Wait extends Component {
    render () {
        return (<div>
          <div>Waiting for player 1 to solve</div>
          <div>
            <button onClick={this.props.onTimeout}
                    disabled={!this.props.isTimeOut}>
              Timeout
            </button>
           </div>
        </div>);
    }
}

//<Move move="0" onMoveChange={} />
class Move extends Component {
    constructor (props) {
        super(props);
    }

    handleChange (i) {
        this.props.onMoveChange(i);
    }

    render () {
        const self = this;
        return (<div> 
            {MOVES.map((m,i)=>
               (<div key={i}>
                 <input type="radio" name="move" checked={self.props.move===i} onChange={self.handleChange.bind(self, i)}/>
                 <label htmlFor={'move'+i}>{m}</label>
               </div>)
            )}
         </div>);
    }
}

function start () {
    var game;
    if (/^#player2\//.test(location.hash)) {
        const state = new RPSState(2);

        const m = location.hash.match(/#player2\/(0x[0-9a-fA-F]+)/);
        const contractAddress = m[1];
        state.setContractAddress(contractAddress);

        state.setTransitioning(true);
        state.sync();
        game = <RPSGame2 state={state} />;

    } else {
        const state = new RPSState(1);
        game = <RPSGame1 state={state} />;
    }
    ReactDOM.render(game, document.getElementById('root'));
}
start();

