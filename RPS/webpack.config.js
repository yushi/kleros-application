const path = require('path');

module.exports = {
    entry: './index.jsx',
    output: {
        filename: 'main.js',
        path: path.resolve(__dirname, 'dist')
    },
    module: {
        rules: [
            {
                test: /\.sol$/,
                use: ['json-loader', path.resolve('./solc-loader.js') ]
            },
            {
                test: /\.jsx$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: { presets: ['babel-preset-react']}
                }
            }
        ]
    },
    devServer: {
        contentBase: './dist'
    },

    watch: true,
    watchOptions: {
        ignored: /node_modules/
    },

    mode: process.env.WEBPACK_MODE || 'development'
};
