export function getMetamaskAddress () {
    // Supposedly one should use web3.eth.accounts[0] according to doc 
    // https://github.com/MetaMask/faq/blob/master/DEVELOPERS.md .
    // But it seems that API is not compatible with web3 1.0 and throws
    // so here is the hack:
    return web3.currentProvider.publicConfigStore.getState().selectedAddress;
}

export function readContractProperty (contract, propertyName) {
    return contract.methods[propertyName]().call();
}

// return a promise that resolves after msecs
export function sleep (msecs) {
    return new Promise(function (resolve) {
        setTimeout(resolve, msecs);
    });
}
